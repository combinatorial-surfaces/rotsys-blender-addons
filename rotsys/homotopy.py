#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from __future__ import print_function

from . import MemHE as _MemHE

try:
    from numpy import empty as _ntbl
except ImportError:
    print("WARNING: numpy not available, reverting to initialized tables\n"
          "         O(n+k1+k2) time for homotopy testing is not guaranteed\n"
          "         anymore.")
    def _ntbl(l, dtype): return [ dtype() ] * l



def contract(component, contract_attr = "contracted"):
    l = list(component)
    for e in component.spanning_tree():
        setattr(e.inv, contract_attr, True)
        setattr(e,     contract_attr, False)
    n = None
    for e in l:
        a = getattr(e, contract_attr, None)
        if a is None:
            n = e
        else:
            if a:
                e.contract_edge()
            else:
                setattr(e, contract_attr, True)
    return None if n is None else n.connected_component()

def contract_copy(component,
                  forwardref = "contracted",
                  backref = None,
                  cls = None):

    component.copy(forwardref = forwardref,
                   backref = backref,
                   cls = cls)

    for e in component.spanning_tree():
        getattr(e, forwardref).contract_edge()
        setattr(e, forwardref, None)
        setattr(e.inv, forwardref, None)

    copies = (getattr(e, forwardref) for e in component)
    copy = next((c for c in copies if c is not None), None)

    return None if copy is None else copy.connected_component()


def _make_radial(component, radial_attr, cls):

    for es in component.dual(backref = "primal",
                             cls = _MemHE).spanning_tree():
        e = es.primal if hasattr(es, "primal") else es.inv.primal
        setattr(e, radial_attr, (True, None))
        setattr(e.inv, radial_attr, (True, None))

    try:
        start = next(e for e in component if not hasattr(e, radial_attr))
    except StopIteration:
        return None

    if cls is None: cls = type(component.anchor)

    def cycle_t():
        e = start
        side = 1
        cr = None
        while True:
            if not getattr(e, radial_attr, (False, None))[0]:
                if side == 1:
                    setattr(e, radial_attr, (False, cr))
                    if e == start and cr is not None: break
                side *= e.sig
                e = e.inv
                cr = cls()
                yield cr.inv
                if side == -1:
                    setattr(e, radial_attr, (False, cr))
            else:
                setattr(e, radial_attr, (True, cr))
            e = e.rot[-side]

    def cycle_s():
        for e in start.neighbour_cycle():
            b, r = getattr(e, radial_attr)
            if not b:
                yield r

    cls.set_neighbour_cycles(cycle_t(), cycle_s())

    return getattr(start, radial_attr)[1]

class _Radial(_MemHE.ConnectedComponent):
    __slots__ = ()

    @classmethod
    def project_path(cls, p):
        for e in p:
            r1, r2 = cls.project_edge(e)
            yield r1
            yield r2

    @classmethod
    def project_edge(cls, e):
        radial_attr = cls._radial_attr
        b, r1 = getattr(e, radial_attr)
        if b or (e.sig == -1):
            _, r2 = getattr(e.inv, radial_attr)
            return (r1, r2.inv)
        else:
            _, r2 = getattr(e.inv, radial_attr)
            return (r1, r2.rot[-1].inv)

def radial(component, radial_attr = "radial", cls = None):
    """Compute the radial graph of an embedded graph with *one* vertex"""

    anchor = _make_radial(component = component,
                          radial_attr = radial_attr,
                          cls = cls)
    if anchor is None: return None

    class Radial(_Radial, type(anchor).ConnectedComponent):
        __slots__ = ()
        _radial_attr = radial_attr

    return Radial(anchor)



class RegionVertex(object):
    __slots__ = ()

    def next(v, e):
        raise NotImplementedError

    def data(v, e):
        raise NotImplementedError

    def join(v, w, e):
        raise NotImplementedError

    def mirror(v, e):
        v_= type(v)()
        v.join(v_, e)

        for d in (-1, 1):
            v0, v_0, e0, d0 = v, v_, e, d
            while True:
                e01 = e0.rot[d0]
                v1 = v0.next(e01)
                if v1 is None: break
                e_01 = e0.inv.rot[-d0 * e0.sig]
                d1 = d0 * e01.sig
                e1 = e01.inv.rot[d1]
                v_1 = type(v)()
                v_0.join(v_1, e_01)
                v1.join(v_1, e1)
                v0, v_0, e0, d0 = v1, v_1, e1, d1

        return v_


class Region(object):
    Vertex = RegionVertex

    def new(self):
        return self.Vertex()

    @staticmethod
    def next(v, e):
        return v.next(e)

    @staticmethod
    def data(v, e):
        return v.data(e)

    @staticmethod
    def join(v, w, e):
        v.join(w, e)

    def extend(self, path, start):
        cur = start
        for e in path:
            n = cur.next(e)
            if n is None: n = cur.mirror(e)
            cur = n
        return cur


class O1Vertex(RegionVertex):
    __slots__ = ("_lookup", "_edges")

    class Edge(object):
        __slots__ = ("index", "endpoint", "data")
        def __new__(cls, index, endpoint, data):
            e = super(O1Vertex.Edge, cls).__new__(cls)
            e.index = index
            e.endpoint = endpoint
            e.data = data
            return e
        def __repr__(e):
            return "E({},v{})".format(e.index, e.endpoint.uid)

    maxindex = 0

    def __new__(cls):
        n = super(O1Vertex, cls).__new__(cls)
        n._lookup = _ntbl(cls.maxindex, dtype = int)
        n._edges = []
        return n

    @staticmethod
    def getindex(e):
        return e.index

    def lookup(v, e):
        i = v.getindex(e)
        if i < 0: return None
        try:
            x = v._edges[v._lookup[i]]
        except IndexError:
            return None
        if x.index != i: return None
        return x

    def next(v, e):
        x = v.lookup(e)
        if x is None: return None
        return x.endpoint

    def data(v, e):
        x = v.lookup(e)
        if x is None: return None
        return x.data

    def join(v, w, e):
        if v.lookup(e) is not None:
            raise ValueError("lift already there")
        i = v.getindex(e)
        d = {}
        v._edges.append(v.Edge(i, w, d))
        v._lookup[i] = len(v._edges) - 1
        w._edges.append(w.Edge(i, v, d))
        w._lookup[i] = len(w._edges) - 1


class O1Region(Region):
    def __init__(self, radial):

        H = list(radial.anchor.neighbour_cycle())
        for i,e in enumerate(H):
            e.index = e.inv.index = i

        class Vertex(O1Vertex):
            __slots__ = ()
            maxindex = len(H)

        self.Vertex = Vertex
        self.origin = Vertex()

    def extend(self, path, start = None):
        return super(O1Region, self).extend(
                path,
                start if start is not None else self.origin)


class _Preprocessed(_Radial, type(anchor).ConnectedComponent):
    __slots__ = ()

    @classmethod
    def project_path(cls, p):
        for e in p:
            if getattr(e, cls._contract_attr, False): continue
            r1, r2 = cls.project_edge(e)
            yield r1
            yield r2

    def is_null_homotopic(self, path, regioncls = O1Region):
        region = regioncls(self)
        return region.extend(self.project_path(path)) == region.origin


def preprocess(component,
               contract_attr = "contracted",
               radial_attr = "radial",
               cls = None):
    contract = contract_copy(component,
                             forwardref = contract_attr,
                             cls = _MemHE)
    if contract is None: return None

    anchor = _make_radial(component = contract,
                          radial_attr = "radial",
                          cls = cls)
    if anchor is None: return None

    for e in component:
        e1 = getattr(e, contract_attr)
        setattr(e, contract_attr, e1 is None)
        if e1 is not None:
            setattr(e, radial_attr, e1.radial)

    class Preprocessed(_Preprocessed, type(anchor).ConnectedComponent):
        __slots__ = ()
        _radial_attr = radial_attr
        _contract_attr = contract_attr

    return Preprocessed(anchor)
