#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from collections import deque as _deque
from itertools import chain as _chain

class HalfEdge(object):
    "The base class of all rotation systems"

    __slots__ = ()

    class rot(object):
        "Proxy attribute for reading/writing local rotational permutation"

        __slots__ = ("owner",)

        def __init__(self, owner):
            self.owner = owner

        def __getitem__(self, direction):
            if direction != 1 and direction != -1:
                raise IndexError("direction must be +/-1")
            return type(self.owner).__getrot__(self.owner, direction)

        def __setitem__(self, direction, value):
            if direction != 1 and direction != -1:
                raise IndexError("direction must be +/-1")
            if not hasattr(type(self.owner), "__setrot__"):
                raise TypeError("'{}' object does not support neighbour "
                                "assignment".format(type(self.owner).__name__))
            type(self.owner).__setrot__(self.owner, direction, value)
    rot = property(rot)

    def __getrot__(self, direction):
        raise NotImplementedError("'{}' class should implement "
                                  "__getrot__()".format(type(self).__name__))

    @property
    def inv(self):
        raise NotImplementedError("'{}' class should have a property"
                                  "inv".format(type(self).__name__))

    @property
    def sig(self):
        raise NotImplementedError("'{}' class should have a property"
                                  "sig".format(type(self).__name__))

    class NeighbourCycle(object):
        __slots__ = ("anchor",)

        def __init__(self, anchor):
            self.anchor = anchor

        def __iter__(self):
            e = self.anchor
            while True:
                yield e
                e = e.rot[1]
                if e == self.anchor: break

        def __reversed__(self):
            e = self.anchor
            while True:
                e = e.rot[-1]
                yield e
                if e == self.anchor: break

    def neighbour_cycle(self):
        return type(self).NeighbourCycle(self)


    class FacialWalk(object):
        __slots__ = ("anchor", "side")

        def __init__(self, anchor, side):
            self.anchor = anchor
            self.side = side

        def __iter__(self):
            e = e0 = self.anchor
            s = s0 = self.side
            while True:
                s *= e.sig
                e = e.inv.rot[-s]
                yield (e,s)
                if (e,s) == (e0, s0): break

    def facial_walk(self, side):
        return type(self).FacialWalk(self, side)


    class ConnectedComponent(object):
        __slots__ = ("anchor",)

        def __init__(self, anchor):
            self.anchor = anchor

        def generic_iterator(self,
                             visited,
                             depth_first = False,
                             rot_dir = 1):
            pending = _deque((self.anchor,))
            while pending:
                e = pending.pop()
                if visited(e): continue
                yield e

                n = e.rot[-rot_dir]
                i = e.inv
                pending.extend( (n, i) if depth_first else (i, n) )

        def iter(self,
                 depth_first = False,
                 rot_dir = 1,
                 cleanup = None):
            try:
                for e in self.generic_iterator(
                        visited = lambda e: getattr(e, "_visited", False),
                        depth_first = depth_first,
                        rot_dir = rot_dir):
                    yield e
                    e._visited = True
            finally:
                def visited(e): return not getattr(e, "_visited", False)
                if cleanup is None:
                    for e in self.generic_iterator(
                            visited     = visited,
                            depth_first = depth_first,
                            rot_dir     = rot_dir):
                        del e._visited
                else:
                    for e in self.generic_iterator(
                            visited     = visited,
                            depth_first = depth_first,
                            rot_dir     = rot_dir):
                        del e._visited
                        cleanup(e)

        def __iter__(self):
            return self.iter()

        def spanning_tree(self,
                          depth_first = False,
                          rot_dir = 1):
            def visit_vertex(e):
                for x in e.neighbour_cycle():
                    x._vertex_visited = True
            def cleanup(e):
                if getattr(e.inv, "_vertex_visited", False):
                    for x in e.inv.neighbour_cycle():
                        del x._vertex_visited

            visit_vertex(self.anchor)
            for e in self.iter(
                    depth_first = depth_first,
                    rot_dir = rot_dir,
                    cleanup = cleanup):
                if not getattr(e.inv, "_vertex_visited", False):
                    yield e
                    visit_vertex(e.inv)

        def facial_walks(self):
            attrs = ["_face_-1_visited", "", "_face_1_visited"]
            pending = _deque()
            try:
                pending.append((self.anchor,1))
                while pending:
                    e0, s0 = pending.pop()
                    if getattr(e0, attrs[s0 + 1], False): continue

                    yield e0.facial_walk(s0)
                    for e,s in e0.facial_walk(s0):
                        setattr(e, attrs[s + 1], True)
                        setattr(e.inv, attrs[-s * e.sig + 1], True)
                        pending.append((e.inv, s * e.sig))

            finally:
                # cleanup
                pending.append((self.anchor,1))
                while pending:
                    e0, s0 = pending.pop()
                    if not getattr(e0, attrs[s0 + 1], False): continue

                    for e,s in e0.facial_walk(s0):
                        delattr(e, attrs[s + 1])
                        delattr(e.inv, attrs[-s * e.sig + 1])
                        pending.append((e.inv, s * e.sig))

        def copy(self,
                 forwardref = None,
                 backref = None,
                 cls = None):
            if cls is None: cls = type(self.anchor)

            fref = forwardref if forwardref is not None else "_copy"

            def copy_iter(se):
                for e in se.neighbour_cycle():
                    if hasattr(e, fref):
                        c = getattr(e, fref)
                    else:
                        c = cls(sig = e.sig)
                        setattr(e, fref, c)
                        setattr(e.inv, fref, c.inv)
                        if backref is not None:
                            setattr(c, backref, e)
                            setattr(c.inv, backref, e.inv)
                    yield c

            try:
                cls.set_neighbour_cycles(copy_iter(self.anchor))
                for e in self.spanning_tree():
                    cls.set_neighbour_cycles(copy_iter(e.inv))
                return getattr(self.anchor, fref).connected_component()

            finally:
                if forwardref is None:
                    for e in self.generic_iterator(
                            lambda e: not hasattr(e, fref)):
                        delattr(e, fref)

        def dual(self,
                 forwardref = None,
                 backref = None,
                 cls = None):
            if cls is None: cls = type(self.anchor)

            fref = "_dual" if forwardref is None else forwardref

            def dual_iter(w):
                for e, s in w:
                    if hasattr(e, fref):
                        # We have setup the forward ref only in the
                        #  direction of the first traversal. If fref
                        #  is set we are traversing twice in the same
                        #  direction and the dual edge has signature -1
                        d = getattr(e, fref)
                        d.sig = -1
                        # Now set fref
                        setattr(e.inv, fref, d.inv if e.sig == 1 else d)
                    elif hasattr(e.inv, fref):
                        dinv = getattr(e.inv, fref)
                        d = dinv.inv if e.sig == 1 else dinv
                        setattr(e, fref, d)
                    else:
                        d = cls()
                        setattr(e, fref, d)
                        if not backref is None:
                            setattr(d, backref, e)
                            if e.sig == 1: setattr(d.inv, backref, e.inv)
                    yield d if s == 1 else d.inv

            try:
                for w in self.facial_walks():
                    cls.set_neighbour_cycles(dual_iter(w))

                return getattr(self.anchor, fref).connected_component()

            finally:
                if forwardref is None:
                    for e in self.generic_iterator(
                            lambda e: not hasattr(e, fref)):
                        delattr(e, fref)


    def connected_component(self):
        return type(self).ConnectedComponent(self)


class MemHE(HalfEdge):
    def __getrot__(self, dir):
        return self.next if dir == 1 else self.prev

    def __setrot__(self, dir, val):
        if dir == 1:
            self.next = val
        else:
            self.prev = val

    def proxy(name):
        def getx(self):
            return vars(self)[name]
        def setx(self, value):
            if not isinstance(value, type(self)):
                raise TypeError("trying to set '{}' object as {} for "
                                "'{}' object".format(type(value).__name__,
                                                     name,
                                                     type(self).__name__))
            vars(self)[name] = value
        return property(getx, setx)
    next = proxy("next")
    prev = proxy("prev")
    del proxy

    @property
    def inv(self):
        return vars(self)["inv"]

    @property
    def sig(self):
        return vars(self)["sig"]

    @sig.setter
    def sig(self, value):
        if not (value == 1 or value == -1):
            raise ValueError("edge signature must be +/-1")
        vars(self)["sig"] = vars(self.inv)["sig"] = value

    def __init__(self, sig = 1, inv = None, **kwds):
        super(MemHE, self).__init__(**kwds)

        self.next = self.prev = self
        if inv is None:
            inv = type(self)(sig = sig, inv = self)
        elif not isinstance(inv, type(self)):
            raise TypeError("trying to set '{}' object as inv for "
                            "'{}' object".format(type(inv).__name__,
                                                 type(self).__name__))
        vars(self)["inv"] = inv

        self.sig = sig

    def attach(self, other, direction = 1):
        self.prev.next = self.next
        self.next.prev = self.prev

        self.rot[-direction] = other
        self.rot[direction] = other.rot[direction]
        self.rot[-direction].rot[direction] = self
        self.rot[direction].rot[-direction] = self

    def detach(self):
        self.prev.next = self.next
        self.next.prev = self.prev
        self.next = self.prev = self

    def delete_edge(self):
        self.detach()
        self.inv.detach()

    def contract_edge(self):
        if self.next is self: return self.inv.detach()
        if self.inv.next is self.inv: return self.detach()
        if self.sig == -1:
            invcycle = list(reversed(self.neighbour_cycle()))
            for e in invcycle: e.sig *= -1
            self.set_neighbour_cycles(invcycle)
        self.prev.next = self.inv.next
        self.next.prev = self.inv.prev
        self.inv.prev.next = self.next
        self.inv.next.prev = self.prev
        self.next = self.prev = self
        self.inv.next = self.inv.prev = self.inv

    @classmethod
    def set_neighbour_cycles(cls, *cycles):
        for cycle in cycles:
            iterator = iter(cycle)
            first = next(iterator)
            first.detach()
            prev = first
            for cur in iterator:
                cur.detach()
                prev.next = cur
                cur.prev = prev
                prev = cur

            prev.next = first
            first.prev = prev

    @classmethod
    def compute_dual(cls, component,
                     forwardref = None, backref = None):
        return component.dual(forwardref = forwardref,
                              backref    = backref,
                              cls        = cls)

    @classmethod
    def copy_component(cls, component,
                       forwardref = None, backref = None):
        return component.copy(forwardref = forwardref,
                              backref    = backref,
                              cls        = cls)


class VertexHE(MemHE):
    class Vertex(object):
        pass

    # Tarjan-inpired system to get to vertex
    def _vertex_owner(self):
        d = vars(self)
        if "vertex" not in d:
            d["vertex"] = (True, None)
        root, x = d["vertex"]
        if root: return self
        owner = x._vertex_owner()
        d["vertex"] = (False, owner)
        return owner

    @property
    def vertex(self):
        d = vars(self._vertex_owner())
        v = d["vertex"][1]
        if v is None:
            v = type(self).Vertex()
            d["vertex"] = (True, v)
        return v

    def attach(self, other, d = 1):
        MemHE.attach(self, other, d)
        vars(self)["vertex"] = (False, other)

    def detach(self):
        MemHE.detach(self)
        d = vars(self)
        if "vertex" in d:
            del d["vertex"]

    def contract_edge(self):
        o1 = self._vertex_owner()
        o2 = self.inv._vertex_owner()
        if o2 is not o1: vars(o2)["vertex"] = (False, o1)
        super(VertexHE, self).contract_edge()

    @classmethod
    def set_neighbour_cycles(cls, *cycles):
        for cycle in cycles:
            i = iter(cycle)
            f = next(i)
            super(VertexHE, cls).set_neighbour_cycles(_chain( (f,), i ))
            for e in f.neighbour_cycle():
                if e is not f: vars(e)["vertex"] = (False, f)

    class ConnectedComponent(MemHE.ConnectedComponent):
        __slots__ = ()

        def spanning_tree(self,
                          depth_first = False,
                          rot_dir = 1):
            def cleanup(e):
                if getattr(e.inv.vertex, "_visited", False):
                    del e.inv.vertex._visited

            self.anchor.vertex._visited = True
            for e in self.iter(
                    depth_first = depth_first,
                    rot_dir = rot_dir,
                    cleanup = cleanup):
                if not getattr(e.inv.vertex, "_visited", False):
                    yield e
                    e.inv.vertex._visited = True

