#!/usr/bin/env python3
# -*- coding: utf-8 -*-

bl_info = {
    "name": "Homotopy",
    "description": "Implements homotopy tests and morphing",
    "author": "Julien Rivaud",
    "version": (1, 0),
    "blender": (2, 6, 2),
    "location": "View3D > Tools > Homotopy",
    "category": "Topology"}


import bpy

from . import bridge
from rotsys import homotopy

class CycleItem(bpy.types.PropertyGroup):
    name = bpy.props.StringProperty(
            name = "Subpath",
            default = "")
    direction = bpy.props.EnumProperty(
            items = [("forward", "Forward", "The path will be used as-is"),
                     ("reverse", "Reverse", "The inverse path will be used")],
            name = "Direction",
            description = "The direction this subpath will be followed in",
            default = "forward")

class EditCycle():
    bl_options = {"INTERNAL"}

    cyclenum = bpy.props.IntProperty(options = { "HIDDEN", "SKIP_SAVE" })

    def execute(self, context):
        return self.run_op(
                getattr(context.owner, "cycle{}".format(self.cyclenum)),
                context)

class MoveDelPath(EditCycle):
    pos = bpy.props.IntProperty(options = { "HIDDEN", "SKIP_SAVE" })

class DelPath(MoveDelPath, bpy.types.Operator):
    """Remove the subpath from the cycle"""
    bl_idname = "topology.homotopy_delpath"
    bl_label = "Delete subpath"

    def run_op(self, cycle, _):
        cycle.remove(self.pos)
        return {'FINISHED'}

class MovePath(MoveDelPath, bpy.types.Operator):
    """Move the subpath in the cycle"""
    bl_idname = "topology.homotopy_movepath"
    bl_label = "Move subpath"

    dir = bpy.props.EnumProperty(
            items = [("up", "Up", ""), ("down", "Down", "")],
            options = { "HIDDEN", "SKIP_SAVE" })

    def run_op(self, cycle, _):
        pos = self.pos
        if self.dir == "up":
            if pos <= 0: return {"CANCELLED"}
            cycle.move(pos, pos - 1)
        else:
            if pos + 1 >= len(cycle): return {"CANCELLED"}
            cycle.move(pos, pos + 1)

        return {'FINISHED'}

class AddPath(EditCycle, bpy.types.Operator):
    """Append a new subpath to the cycle"""
    bl_idname = "topology.homotopy_addpath"
    bl_label = "Append subpath"

    def run_op(self, cycle, context):
        subpath = bpy.data.objects.get(
                getattr(context.owner, "subpath{}".format(self.cyclenum)),
                None)

        if (subpath not in context.surface.children) or \
                (subpath.type != "CURVE"):
            return { "CANCELLED"}

        item = cycle.add()
        item.name = subpath.name

        return {'FINISHED'}

class Homotopy(bpy.types.Operator):
    """This operator tests if two cycles are homotopic on a surface"""
    bl_idname = "topology.homotopy"
    bl_label = "Homotopy test"
    bl_options = {"REGISTER", "UNDO"}

    mode = bpy.props.EnumProperty(
            items = [("contract-test", "Contractibility",
                      "Test for null-homotopy of a given cycle"),
                     ("contract-build", "Cycle contraction",
                      "Build an explicit homotopy between a cycle and"
                      "the null cycle"),
                     ("homotopy-test", "Homotopy testing",
                      "Test if two cycles are homotopic"),
                     ("homotopy-build", "Homotopy construction",
                      "Build an explicit homotopy between two cycles")],
            name = "Mode of operation",
            description = "The type of action to be taken by "
                          "the homotopy operator",
            default = "contract-test")

    cycle1 = bpy.props.CollectionProperty(
            type = CycleItem,
            name = "First cycle",
            description = "The first cycle; a concatenation of curves")

    cycle2 = bpy.props.CollectionProperty(
            type = CycleItem,
            name = "Second cycle",
            description = "The second cycle; a concatenation of curves")

    def subpath_gen(txt):
        def f(self, context):
            surf = context.object
            l = [("", "", "")]
            if surf is not None:
                l.extend( (o.name, o.name, "") for o in surf.children
                                               if o.type == "CURVE" )
            return l
        return bpy.props.EnumProperty(
            items = f,
            name = "Subpath",
            description = "The subpath to append to "
                          "the {}cycle".format(txt),
            options = { "HIDDEN", "SKIP_SAVE" })
    subpath1 = subpath_gen("")
    subpath2 = subpath_gen("second ")
    del subpath_gen


    def execute(self, context):
        surface = context.object

        if not hasattr(self, "graph"):
            self.graph = bridge.from_mesh(surface.data)
            self.preprocessed = homotopy.preprocess(self.graph.component())

        graph = self.graph
        preprocessed = self.preprocessed

        if preprocessed is None: return {'FINISHED'}


        return {'FINISHED'}


    @classmethod
    def poll(cls, context):
        obj = context.object
        return obj is not None and \
                obj.type == "MESH" and \
                obj.is_visible(context.scene)

    def invoke(self, context, event):
        self.surface = context.object.name
        return self.execute(context)

    def draw(self, context):
        layout = self.layout

        layout.label("Combinatorial surface:")
        sub = layout.row()
        sub.alignment = "CENTER"
        sub.label(self.surface, icon = "MESH_DATA")

        layout.separator()

        layout.label("Operation:")
        layout.prop(data = self, property = "mode", text = "")

        for num, text in enumerate(
                ("Cycle:",) if self.mode.startswith("contract")
                else ("First cycle:", "Second cycle:") ):
            num += 1

            layout.separator()
            block = layout.column()
            block.context_pointer_set("owner", self)
            block.label(text)
            cbox = block.box()
            cycle = getattr(self, "cycle{}".format(num))
            for i, c in enumerate(cycle):
                row = cbox.row()
                direct = row.row(align = True)
                direct.scale_x = 0.1
                direct.prop_enum(data = c, property = "direction",
                                 value = "forward",
                                 icon = "ZOOMIN", text = "")
                direct.prop_enum(data = c, property = "direction",
                                 value = "reverse",
                                 icon = "ZOOMOUT", text = "")
                row.label(c.name)
                updn = row.row(align = True)
                sub = updn.row()
                sub.active = i > 0
                op = sub.operator(MovePath.bl_idname, text="",
                                  icon="TRIA_UP")
                op.cyclenum = num
                op.pos = i
                op.dir = "up"
                sub = updn.row()
                sub.active = i + 1 < len(cycle)
                op = sub.operator(MovePath.bl_idname, text="",
                                  icon="TRIA_DOWN")
                op.cyclenum = num
                op.pos = i
                op.dir = "down"

                op = row.operator(DelPath.bl_idname, text="",
                                  icon="X")
                op.cyclenum = num
                op.pos = i
            end = block.row(align = True)
            end.prop(data = self, property = "subpath{}".format(num),
                    text="")
            op = end.row()
            surface = bpy.data.objects[self.surface]
            op.context_pointer_set("surface", surface)
            subpath = bpy.data.objects.get(
                    getattr(self, "subpath{}".format(num)),
                    None)
            op.active = (subpath in surface.children) and \
                        (subpath.type == "CURVE")
            op.operator(AddPath.bl_idname, text="",
                        icon="ZOOMIN").cyclenum = num

class Panel(bpy.types.Panel):
    bl_label = "Homotopy"
    bl_idname = "MESH_PT_homotopy"
    bl_space_type = "VIEW_3D"
    bl_region_type = "TOOLS"

    def draw(self, context):
        self.layout.operator(Homotopy.bl_idname)

def register():
    bpy.utils.register_module(__name__)

def unregister():
    bpy.utils.unregister_module(__name__)

if __name__ == "__main__":
    register()
