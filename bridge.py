#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import bmesh as _bmesh
from rotsys import HalfEdge as _HE

class BlenderHE(_HE):
    __slots__ = ("bmesh_edge", "bmesh_vert", "__dict__")

    def __new__(cls, bmesh_edge, bmesh_vert):
        n = super().__new__(cls)
        n.bmesh_edge = bmesh_edge
        n.bmesh_vert = bmesh_vert
        return n

    sig = 1

    @property
    def inv(self):
        e = self.bmesh_edge
        return type(self)(
                bmesh_edge = e,
                bmesh_vert = e.other_vert(self.bmesh_vert))

    def __getrot__(self, d):
        v = self.bmesh_vert
        ll = next( ll for ll in self.bmesh_edge.link_loops
                        if ll.vert == v )
        e = ll.link_loop_next.edge
        if v not in e.verts:
            e = ll.link_loop_prev.edge
        return type(self)(bmesh_edge = e, bmesh_vert = v)

    def __eq__(self, other):
        return self.bmesh_edge == other.bmesh_edge and \
                self.bmesh_vert == other.bmesh_vert

    class ConnectedComponent(_HE.ConnectedComponent):
        __slots__ = ()

        def spanning_tree(self,
                          depth_first = False,
                          rot_dir = 1):
            def cleanup(e): e.inv.bmesh_vert.tag = False
            self.anchor.bmesh_vert.tag = True
            for e in self.iter(
                    depth_first = depth_first,
                    rot_dir = rot_dir,
                    cleanup = cleanup):
                if not e.inv.bmesh_vert.tag:
                    yield e
                    e.inv.bmesh_vert.tag = True


class from_mesh(object):
    def __init__(self, mesh, HE = None):
        if HE is None:
            dicts = {}
            class HE(BlenderHE):
                def __new__(cls, bmesh_edge, bmesh_vert):
                    n = super().__new__(cls, bmesh_edge, bmesh_vert)
                    k = (bmesh_edge, bmesh_vert)
                    if k in dicts:
                        n.__dict__ = dicts[k]
                    else:
                        dicts[k] = n.__dict__
                    return n
        self.bmesh = _bmesh.new()
        self.bmesh.from_mesh(mesh)
        self.HE = HE

    def component(self, v = 0):
        if isinstance(v, int): v = self.bmesh.verts[v]
        return self.HE(v.link_edges[0], v).connected_component()

    def project_point(self, co):
        return min(self.bmesh.verts,
                   key = lambda w: (w.co - co).length_squared)

    def project(self, it):
        it = iter(it)
        cur = self.project_point(next(it))
        for co in it:
            e = min(cur.link_edges,
                    key = lambda e:
                            (e.other_vert(cur).co - co).length_squared)
            yield self.HE(e, cur)
            cur = e.other_vert(cur)
